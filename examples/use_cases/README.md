# Use cases

This folder contains the use cases:

- [Compressible potential flow problem](compressible_potential_flow/README.md)
- [Fluid dynamics building problem](fluid_dynamics_building)
- [Wind engineering rectangle problem](wind_engineering_rectangle)
    - [Deterministic wind engineering rectangle problem with ensemble average approach](wind_engineering_rectangle/deterministic_ensemble_average)
    - [Stochastic wind engineering rectangle problem](wind_engineering_rectangle/stochastic_MC)
    - [Stochastic wind engineering rectangle problem with ensemble average approach](wind_engineering_rectangle/stochastic_MC_ensemble_average)
