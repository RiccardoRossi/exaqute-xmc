# ExaQUte XMC Examples

This folder contains a collection of validation and use cases showcasing different features of [ExaQUte XMC](https://gitlab.com/RiccardoRossi/exaqute-xmc).

The organization is the following:

- **Use cases**: complete cases showcasing features or challenging applications,

- **Validation**: benchmark problems (academic or otherwise).

Each case should be self-contained and should also be accompanied by a page presenting it and linking to reference results, if available in the literature. However, to avoid having large files here, it may be necessary downloading files from an external repository to run the corresponding example.

Unit tests should *not* be uploaded to this directory. Please put them in the `tests`.


## Built-in examples

**Validation Cases**
- [Gaussian benchmark](validation/gaussian_benchmark)


## Kratos-Multiphysics examples

**Use Cases**
- [Compressible potential flow problem](use_cases/compressible_potential_flow/README.md)
- [Fluid dynamics building problem](use_cases/fluid_dynamics_building)
- [Wind engineering rectangle problem](use_cases/wind_engineering_rectangle)
    - [Deterministic wind engineering rectangle problem with ensemble average approach](use_cases/wind_engineering_rectangle/deterministic_ensemble_average)
    - [Stochastic wind engineering rectangle problem](use_cases/wind_engineering_rectangle/stochastic_MC)
    - [Stochastic wind engineering rectangle problem with ensemble average approach](use_cases/wind_engineering_rectangle/stochastic_MC_ensemble_average)

**Validation Cases**
- [Elliptic benchmark](validation/elliptic_benchmark)


### Remarks
- To run with `PyCOMPSs`, it is necessary to compile it first. You can find a detailed guide on how to do it in the [Kratos wiki](https://github.com/KratosMultiphysics/Kratos/wiki/How-to-run-multiple-cases-using-PyCOMPSs). In addition, the appropriate import has to be changed from
~~~python
from exaqute.ExaquteTaskLocal import *
~~~
to
~~~python
from exaqute.ExaquteTaskPyCOMPSs import *
~~~
in the execution script and in `xmc/distributedEnvironmentFramework.py`.
We refer to the [MultilevelMonteCarloApplication documentation](https://github.com/KratosMultiphysics/Kratos/tree/master/applications/MultilevelMonteCarloApplication#pycompss) for further details.
In case running with `PyCOMPSs` gives errors, try to replace relative paths with absolute paths in configuration `json` files, as first attempt to fix the issue.
- These examples make use of some external libraries that are not compatible with the Kratos binaries. In order to try these examples, it is necessary to compile Kratos on your own machine.