"""
Usage from commande line:
python runme.py {case}

With {case} being one of MC, AMC, MLMC, AMLMC and CMLMC.
Alternatively, edit the default value of the case variable in this script, and run the script without argument.
"""
import sys
import os
import xmc
import numpy as np
from json import load
from typing import List

sys.dont_write_bytecode = True


@xmc.distributedEnvironmentFramework.ExaquteTask()
def abs_Task(value: float) -> float:
    return abs(value)


@xmc.distributedEnvironmentFramework.ExaquteTask()
def multiplyByScalar_Task(value: float, scalar: float) -> float:
    return value * scalar


def continuationTolerances(tol: List[float], ratios: List[float]):
    """
    Computes sequence of tolerances for a continuation algorithm.

    Input:
    - tol: [initial_tolerance, final_tolerance]
    - ratios: [refinement_ratio, post_final_ratio]

    Output:
    - tolerances: [tolerance0, tolerance1, ...]
    """
    number_iterations = int(np.ceil(np.log(tol[0] / tol[-1]) / np.log(ratios[0])))
    tolerances = [
        tol[-1] * (ratios[0] ** (number_iterations - i)) for i in range(number_iterations)
    ]
    tolerances.extend([tol[-1], tol[-1] / ratios[-1]])
    return tolerances


configSet = {
    "MC": (
        "hierarchy_mc",
        "solver_single-level",
        "statistical_estimator_single-level",
        "assemblers_single-level",
        "error_statistical",
        "mc-sampler",
        "multiCriterion_error+iterations",
        "algorithm",
    ),
    "AMC": (
        "hierarchy_amc",
        "solver_single-level",
        "statistical_estimator_single-level",
        "assemblers_single-level",
        "error_statistical",
        "mc-sampler",
        "multiCriterion_error+iterations",
        "algorithm",
    ),
    "MLMC": (
        "hierarchy_mlmc",
        "solver_multi-level",
        "statistical_estimator_multi-level",
        "assemblers_multi-level",
        "error_statistical+bias",
        "mc-sampler",
        "multiCriterion_error+iterations",
        "algorithm",
    ),
    "AMLMC": (
        "hierarchy_amlmc",
        "solver_multi-level",
        "statistical_estimator_multi-level",
        "predictors",
        "assemblers_multi-level",
        "error_statistical+bias",
        "mc-sampler",
        "multiCriterion_error+iterations",
        "algorithm",
    ),
    "CMLMC": (
        "hierarchy_cmlmc",
        "solver_multi-level",
        "statistical_estimator_multi-level",
        "predictors",
        "assemblers_multi-level",
        "error_statistical+bias",
        "mc-sampler",
        "multiCriterion_error+iterations",
        "algorithm",
    ),
}

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    # Choose case
    # Available choices are configSet.keys() (see definition above)
    if len(sys.argv) > 1:
        # Selected from command line
        case = sys.argv[1]
    else:
        # Select by changing the value below
        case = "MC"

    config = {}
    for config_name in configSet[case]:
        with open(f"{config_name}.json", "r") as config_file:
            config.update(load(config_file))

    # Stopping criteria
    error_tol = 0.1
    min_iter = 2
    max_iter = 20
    if case == "CMLMC":
        error_tol = continuationTolerances([5 * error_tol, error_tol], [2, 1.3])
        max_iter = max(max_iter, len(error_tol))
    else:
        # Ensure list
        error_tol = [error_tol]
    # TODO not very robust
    config["multiCriterion"]["criteria"] = [
        xmc.monoCriterion.MonoCriterion(config["multiCriterion"]["criteria"][0], error_tol),
        xmc.monoCriterion.MonoCriterion(config["multiCriterion"]["criteria"][1], [min_iter]),
        xmc.monoCriterion.MonoCriterion(config["multiCriterion"]["criteria"][2], [max_iter]),
    ]
    config["xmcAlgorithm"]["stoppingCriterion"] = xmc.multiCriterion.MultiCriterion(
        **config["multiCriterion"]
    )

    # HierarchyOptimiser
    if "varianceBlender" in config["hierarchyOptimiser"].keys():
        config["hierarchyOptimiser"][
            "varianceBlender"
        ] = xmc.bayesianEstimator.BayesianEstimator(
            **config["hierarchyOptimiser"]["varianceBlender"]
        )
    config["xmcAlgorithm"]["hierarchyOptimiser"] = xmc.hierarchyOptimiser.HierarchyOptimiser(
        **config["hierarchyOptimiser"]
    )

    #
    # MonteCarloSampler
    #
    # Estimation assemblers
    for i, a in enumerate(config["assemblers"]):
        config["assemblers"][i] = xmc.estimationAssembler.EstimationAssembler(**a)
    #
    # Error assemblers
    for i, e in enumerate(config["errorEstimators"]):
        config["errorEstimators"][i] = xmc.errorEstimator.ErrorEstimator(**e)
    #
    # Assign estimation and error assemblers
    for k in (
        "assemblers",
        "estimatorsForAssembler",
        "errorEstimators",
        "assemblersForError",
    ):
        config["monteCarloSampler"][k] = config[k]
    #
    # Model estimators
    if "costPredictor" in config.keys():
        config["monteCarloSampler"]["costPredictor"] = xmc.modelEstimator.ModelEstimator(
            **config["costPredictor"]
        )
        config["monteCarloSampler"]["costEstimatorsForPredictor"] = config[
            "costEstimatorsForPredictor"
        ]
    else:
        # TODO This should not be necessary
        # MonteCarloSampler can detect this itself
        config["monteCarloSampler"]["isCostUpdated"] = False
    if "qoiPredictor" in config.keys():
        for i, p in enumerate(config["qoiPredictor"]):
            config["qoiPredictor"][i] = xmc.modelEstimator.ModelEstimator(**p)
        # Replace strings by callables
        # TODO Fragile
        for i, c in enumerate(config["estimatorsForPredictor"]):
            if c[-1] == "abs_Task":
                config["estimatorsForPredictor"][i][-1] = abs_Task
            elif c[-1] == "multiplyByScalar_Task":
                config["estimatorsForPredictor"][i][-1] = multiplyByScalar_Task
        # Assign estimators
        for k in ("qoiPredictor", "estimatorsForPredictor"):
            config["monteCarloSampler"][k] = config[k]
    #
    # Solver
    for k in ("solverWrapper", "solverWrapperInputDictionary"):
        config["monteCarloSampler"]["indexConstructorDictionary"]["samplerInputDictionary"][
            k
        ] = config[k]
    #
    # Statistical estimators
    for k in ("qoiEstimator", "qoiEstimatorInputDictionary"):
        config["monteCarloSampler"]["indexConstructorDictionary"][k] = config[k]
    #
    # Construction of Monte Carlo Sampler
    config["xmcAlgorithm"]["monteCarloSampler"] = xmc.monteCarloSampler.MonteCarloSampler(
        **config["monteCarloSampler"]
    )

    # XMCAlgorithm
    for k in (
        "predictorsForHierarchy",
        "costPredictorForHierarchy",
        "estimatorsForHierarchy",
        "costEstimatorForHierarchy",
        "tolerancesForHierarchy",
        "errorParametersForHierarchy",
    ):
        if k in config.keys():
            config["xmcAlgorithm"][k] = config[k]
    algo = xmc.XMCAlgorithm(**config["xmcAlgorithm"])

    algo.runXMC()
