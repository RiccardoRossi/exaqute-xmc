# Gaussian benchmark

**Author:** Quentin Ayoul-Guilmard

**Dependencies:** None beside those of XMC.

## Description

This is a basic example, where the statistics of a Gaussian random variable are evaluated with different algorithms. This is meant to demonstrate the modularity of the library, and illustrate the configuration of one's own algorithms.

## Usage

The script to be run is the aptly-named `runme.py`. The choice of algorithm is either written in the script or passed as an argument to the Python interpreter. 
In either case, the possible values are the keys of the dictionary `configSet` defined in `runme.py`, viz.
- `MC` (Monte Carlo), 
- `AMC` (Adaptive MC), 
- `MLMC` (multi-level MC), 
- `AMLMC` (adaptive MLMC) and
- `CMLMC` (continuation MLMC).


### From the script

1. edit `runme.py` to change the value of the `case` variable;
2. run `runme.py`.


### From the shell

Pass the choice of algorithm as an additional argument to the Python interpreter:
```shell
python runme.py $case
```
with `case` being one of the possible values listed above.
