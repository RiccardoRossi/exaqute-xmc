---------------------
Developer guide
---------------------

.. mdinclude:: ../../CONTRIBUTING.md


Building the documentation
==================================

Dependencies:

* sphinx
* sphinx-autodoc-typehints
* sphinx-rtd-theme
* m2r2

To build the documentation in HTML format, in `doc/` run the shell command::

  make html

