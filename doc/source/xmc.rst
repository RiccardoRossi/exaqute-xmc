XMC objects
================

Algorithm
---------------
.. automodule:: xmc.xmcAlgorithm
.. automodule:: xmc.methodDefs_xmcAlgorithm.checkInitialisation
.. automodule:: xmc.methodDefs_xmcAlgorithm.updateTolerance

Stopping criterion
---------------------
.. automodule:: xmc.multiCriterion
.. automodule:: xmc.methodDefs_multiCriterion.flag
.. automodule:: xmc.methodDefs_multiCriterion.interpreter

.. automodule:: xmc.monoCriterion
.. automodule:: xmc.methodDefs_monoCriterion.criterionFunctions

Hierarchy optimisation
--------------------------
.. automodule:: xmc.hierarchyOptimiser
.. automodule:: xmc.methodDefs_hierarchyOptimiser.optimalSampleNumbers
.. automodule:: xmc.methodDefs_hierarchyOptimiser.optimalIndexSet
.. automodule:: xmc.methodDefs_hierarchyOptimiser.updateHierarchySpace

Monte Carlo hierarchy
-------------------------
.. automodule:: xmc.monteCarloSampler
.. automodule:: xmc.methodDefs_monteCarloSampler.asynchronousUpdateGlobalEstimators

.. automodule:: xmc.monteCarloIndex
.. automodule:: xmc.methodDefs_monteCarloIndex.updateEstimators

Global estimation assemblers
----------------------------------
.. automodule:: xmc.estimationAssembler
.. automodule:: xmc.methodDefs_estimationAssembler.assembleEstimation

.. automodule:: xmc.errorEstimator                
.. automodule:: xmc.methodDefs_errorEstimator.errorEstimation

Predictors
------------
.. automodule:: xmc.modelEstimator
.. automodule:: xmc.methodDefs_modelEstimator.update
.. automodule:: xmc.methodDefs_modelEstimator.valueForParameters

.. automodule:: xmc.bayesianEstimator
.. automodule:: xmc.methodDefs_bayesianEstimator.blend
                
Statistical estimators
-------------------------
.. automodule:: xmc.statisticalEstimator

.. automodule:: xmc.momentEstimator
   :show-inheritance:
.. automodule:: xmc.methodDefs_momentEstimator.hStatistics
.. automodule:: xmc.methodDefs_momentEstimator.powerSums
.. automodule:: xmc.methodDefs_momentEstimator.updatePowerSums
.. automodule:: xmc.methodDefs_momentEstimator.updateCombinedPowerSums
.. automodule:: xmc.methodDefs_momentEstimator.computeCentralMoments
.. automodule:: xmc.methodDefs_momentEstimator.computeErrorEstimation

Random sampling
--------------------
.. automodule:: xmc.randomGeneratorWrapper
.. automodule:: xmc.methodDefs_randomGeneratorWrapper.generator

.. automodule:: xmc.sampleGenerator
.. automodule:: xmc.methodDefs_sampleGenerator.qoiProcessor


Solvers interfaces
----------------------
.. automodule:: xmc.solverWrapper

.. automodule:: xmc.classDefs_solverWrapper.singleLevelRNGSolverWrapper
   :show-inheritance:      

.. automodule:: xmc.classDefs_solverWrapper.multiLevelRNGSolverWrapper
   :show-inheritance:      

.. automodule:: xmc.classDefs_solverWrapper.KratosSolverWrapper
   :show-inheritance:      
.. automodule:: xmc.classDefs_solverWrapper.methodDefs_KratosSolverWrapper.simulation_definition

                
Miscellaneous functions
----------------------------
.. automodule:: xmc.tools

      
Framework for distributed computing
-------------------------------------------
.. automodule:: xmc.distributedEnvironmentFramework
   :imported-members:
