.. XMC documentation master file, created by
   sphinx-quickstart on Mon Nov  2 20:38:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to XMC's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_link
   xmc
   references
   changelog_link
   developers
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
