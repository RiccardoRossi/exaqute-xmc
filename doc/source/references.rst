References
===============

Monte-Carlo methods
-------------------------
#. Giles, M. B.
   Multilevel Monte Carlo methods.
   *Acta Numerica*, Cambridge University Press, 2015, 24, 259-328
#. Collier, N.; Haji-Ali, A.-L.; Nobile, F.; von Schwerin, E.; Tempone, R.
   A continuation multilevel Monte Carlo algorithm.
   *BIT Numerical Mathematics*, 2015, 55, 399-432 
#. Krumscheid, S.; Nobile, F.; Pisaroni, M.
   Quantifying uncertain system outputs via the multilevel Monte Carlo method — Part I: Central moment estimation.
   *Journal of Computational Physics*, 2020, 109466.
#. Krumscheid, S.; Nobile, F.
   Multilevel Monte Carlo Approximation of Functions.
   *SIAM/ASA Journal on Uncertainty Quantification8*, 2018, 6, 1256-1293.

Distributed computing
-------------------------
#. Badia, R. M.; Conejero, J.; Diaz, C.; Ejarque, J.; Lezzi, D.; Lordan, F.; Ramon-Cortes, C.; Sirvent, R.
   COMP Superscalar, an interoperable programming framework.
   *SoftwareX*, 2015, 3--4
#. Tejedor, E.; Becerra, Y.; Alomar, G.; Queralt, A.; Badia, R. M.; Torres, J.; Cortes, T.; Labarta, J.
   PyCOMPSs: Parallel computational workflows in Python.
   *International Journal of High Performance Computing Applications*, 2017, 31, 66-82 
