#!/usr/bin/env bash

path_to_test_folder=$(pwd)

materials_path_to_replace_poisson="poisson_square_2d/problem_settings/materials_poisson_square_2d.json"
materials_new_path_poisson="$path_to_test_folder/poisson_square_2d/problem_settings/materials_poisson_square_2d.json"

# set absolute path in Kratos parameters
sed -i "s|$materials_path_to_replace_poisson|$materials_new_path_poisson|g" "poisson_square_2d/problem_settings/parameters_finer.json"

# run

runcompss \
    --lang=python \
    --python_interpreter=python3 \
    ./test_xmcAlgorithm.py

# revert change in Kratos parameters
sed -i "s|$materials_new_path_poisson|$materials_path_to_replace_poisson|g" "poisson_square_2d/problem_settings/parameters_finer.json"
