# These tests should *only* be run in parallel with COMPSs or HyperLoom.
# E.g. runcompss --lang=python --python_interpreter=python3 parallel-only_tests.py

import unittest

if __name__ == "__main__":
    parallel_loader = unittest.TestLoader()
    parallel_loader.testMethodPrefix = "parallel_test_"
    mpi_loader = unittest.TestLoader()
    mpi_loader.testMethodPrefix = "mpi_test_"
    parallel_tests = parallel_loader.discover(".", pattern="*Tests.py")
    parallel_tests.addTest(parallel_loader.discover(".", pattern="test*.py"))
    parallel_tests.addTest(mpi_loader.discover(".", pattern="test*.py"))
    parallel_tests.addTest(mpi_loader.discover(".", pattern="*Tests.py"))
    runner = unittest.runner.TextTestRunner(verbosity=1)
    runner.run(parallel_tests)
