import unittest
import momentEstimatorTests


class SkipCase(unittest.TestCase):
    """
    Dummy class used to mark tests to be skipped.
    """

    def runTest(self):
        raise unittest.SkipTest("Random test: might fail.")

    @unittest.skip("This is a dummy test, ignore its being skipped.")
    def parallel_test_dummy(self):
        """
        This method is only here to prevent accidental discovery of SkipCase.runTest.
        A test still appears as skipped, but the message is less confusing.
        """
        pass


if __name__ == "__main__":
    # Load all momentEstimator tests
    loader = unittest.TestLoader()
    allTests = unittest.TestSuite(loader.loadTestsFromModule(momentEstimatorTests))

    deterministicTests = unittest.TestSuite()
    for test_group in allTests._tests:
        for test in test_group:
            # Exclude random tests
            if "_random" in test._testMethodName:
                testName = test._testMethodName
                setattr(test, testName, getattr(SkipCase(), "runTest"))
            deterministicTests.addTest(test)

    runner = unittest.runner.TextTestRunner()
    runner.run(deterministicTests)
