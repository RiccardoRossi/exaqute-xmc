#!/usr/bin/env bash

path_to_test_folder=$(pwd)

# test_xmcAlgorithm_mpi.py
path_to_replace_caarc="caarc_wind_mpi/"
new_path_caarc="$path_to_test_folder/caarc_wind_mpi/"

sed -i "s|$path_to_replace_caarc|$new_path_caarc|g" "test_xmcAlgorithm_mpi.py"

# caarc problem
path_to_replace_lev0_caarc="problem_settings/CAARC_3d_combinedPressureVelocity_30nodes"
new_path_lev0_caarc="$path_to_test_folder/caarc_wind_mpi/problem_settings/CAARC_3d_combinedPressureVelocity_30nodes"
path_to_replace_lev1_caarc="problem_settings/CAARC_3d_combinedPressureVelocity_36nodes"
new_path_lev1_caarc="$path_to_test_folder/caarc_wind_mpi/problem_settings/CAARC_3d_combinedPressureVelocity_36nodes"
path_to_replace_lev2_caarc="problem_settings/CAARC_3d_combinedPressureVelocity_46nodes"
new_path_lev2_caarc="$path_to_test_folder/caarc_wind_mpi/problem_settings/CAARC_3d_combinedPressureVelocity_46nodes"
materials_path_to_replace_caarc="materials/materials_Re_119M.json"
materials_new_path_caarc="$path_to_test_folder/caarc_wind_mpi/materials/materials_Re_119M.json"

# set absolute path in Kratos parameters
sed -i "s|$path_to_replace_lev0_caarc|$new_path_lev0_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet.json"
sed -i "s|$path_to_replace_lev0_caarc|$new_path_lev0_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev0.json"
sed -i "s|$path_to_replace_lev1_caarc|$new_path_lev1_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev1.json"
sed -i "s|$path_to_replace_lev2_caarc|$new_path_lev2_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev2.json"
sed -i "s|$materials_path_to_replace_caarc|$materials_new_path_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet.json"
sed -i "s|$materials_path_to_replace_caarc|$materials_new_path_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev0.json"
sed -i "s|$materials_path_to_replace_caarc|$materials_new_path_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev1.json"
sed -i "s|$materials_path_to_replace_caarc|$materials_new_path_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev2.json"

# run

export OMP_NUM_THREADS=1
export computing_units_mlmc_execute_0=1;
export computing_procs_mlmc_execute_0=2;
export computing_units_mlmc_execute_1=1;
export computing_procs_mlmc_execute_1=2;
export computing_units_mlmc_execute_2=1;
export computing_procs_mlmc_execute_2=4;

runcompss \
    --lang=python \
    --cpu_affinity="disabled" \
    --python_interpreter=python3 \
    ./test_xmcAlgorithm_mpi.py TestXMCAlgorithmMPI

# mpirun -n 2 python3 test_xmcAlgorithm_mpi.py

# revert change in Kratos parameters

# test_xmcAlgorithm_mpi.py
sed -i "s|$new_path_caarc|$path_to_replace_caarc|g" "test_xmcAlgorithm_mpi.py"
# caarc
sed -i "s|$new_path_lev0_caarc|$path_to_replace_lev0_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet.json"
sed -i "s|$new_path_lev0_caarc|$path_to_replace_lev0_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev0.json"
sed -i "s|$new_path_lev1_caarc|$path_to_replace_lev1_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev1.json"
sed -i "s|$new_path_lev2_caarc|$path_to_replace_lev2_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev2.json"
sed -i "s|$materials_new_path_caarc|$materials_path_to_replace_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet.json"
sed -i "s|$materials_new_path_caarc|$materials_path_to_replace_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev0.json"
sed -i "s|$materials_new_path_caarc|$materials_path_to_replace_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev1.json"
sed -i "s|$materials_new_path_caarc|$materials_path_to_replace_caarc|g" "caarc_wind_mpi/problem_settings/ProjectParametersCAARC_steadyInlet_lev2.json"
