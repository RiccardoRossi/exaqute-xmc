# These tests should *only* be run in parallel with MPI.
# E.g. mpirun -n $number_processes python3 mpi_tests.py

import unittest

if __name__ == "__main__":
    mpi_loader = unittest.TestLoader()
    mpi_loader.testMethodPrefix = "mpi_test_"
    mpi_tests = mpi_loader.discover(".", pattern="test_*.py")
    mpi_tests.addTest(mpi_loader.discover(".", pattern="*Tests.py"))
    runner = unittest.runner.TextTestRunner(verbosity=1)
    runner.run(mpi_tests)
