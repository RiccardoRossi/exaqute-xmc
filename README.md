[CI-image]: https://gitlab.com/RiccardoRossi/exaqute-xmc/badges/master/pipeline.svg
[CI-link]: https://gitlab.com/RiccardoRossi/exaqute-xmc/-/commits/master

[license-image]: https://img.shields.io/badge/license-BSD-green.svg?style=flat
[license]: https://gitlab.com/RiccardoRossi/exaqute-xmc/-/blob/master/LICENSE

[DOI-image]: https://zenodo.org/badge/DOI/10.5281/zenodo.3235832.svg
[DOI]: https://doi.org/10.5281/zenodo.3235832

# XMC

[![Gitlab CI][CI-image]][CI-link] [![License][license-image]][license] [![DOI][DOI-image]][DOI]

**XMC** is a Python library for parallel, adaptive, hierarchical Monte Carlo algorithms, aiming at reliability, modularity, extensibility and high performance.

**XMC** is developed within the [ExaQUte](http://exaqute.eu/) European H2020 project.

**XMC** is **free** under BSD-4 license.

## Main Features
The algorithms **XMC** can run include:
- Monte Carlo,
- Multilevel Monte Carlo,
- Continuation Multilevel Monte Carlo,
- Asynchronous Monte Carlo and Multilevel Monte Carlo.

## Documentation and Usage
Documentation is still a work in progress.
It can be found in the form of docstrings in the code and as HTML pages in `doc/html`. 
If you do not wish to download these files, you may follow [this link](https://glcdn.githack.com/RiccardoRossi/exaqute-xmc/-/raw/635f692168896aed8922b59b64d57143c128b7bf/doc/html/index.html) instead.
This link may be updated more slowly; the files in `doc/html` are the reference.

Some examples and validation benchmarks can be found [here](https://gitlab.com/RiccardoRossi/exaqute-xmc/-/tree/development/examples/).

## Dependencies
- NumPy;
- SciPy.

Optional dependencies for parallel computation:
- [COMPSs](https://github.com/bsc-wdc/compss) 2.8 (including its Python interface PyCOMPSs) or [HyperLoom](https://github.com/It4innovations/HyperLoom);
- MPI.

## External collaborations
**XMC** is integrated with [Kratos Multiphysics](https://github.com/KratosMultiphysics/Kratos) as solver software.

## How to cite XMC?
All the necessary metadata, as well as formatted citations, are provided on the [Zenodo record](http://doi.org/10.5281/zenodo.3235832).
