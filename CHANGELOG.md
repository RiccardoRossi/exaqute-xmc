# Change log
All notable changes to this project will be documented in this file.

The format is based on '[Keep a Changelog](http://keepachangelog.com/)'
and this project adheres to [semantic versioning](http://semver.org/).

## [Unreleased]

### Added
- KratosSolverWrapper supports ensemble average for MultiMomentEstimator and MultiCombinedMomentEstimator. (MR !51)
- KratosSolverWrapper supports MPI for Monte Carlo and Multilevel Monte Carlo. Tests with both runcompss and mpirun are added. (MR !47, !52, !53)
- Integration with ParMmg is added. A test is added and it can be run with both runcompss and mpirun. (MR !53)
- Seed generation for multiple ensembles is added to the `KratosSolverWrapper` class. (MR !57)
- A default Kratos analysis stage is created to generate documentation (fix #44). (MR !44)
- Link to `githack.com` proxy in `README.md`, to display HTML documentation without manual download.

### Changed
- Improved Examples documentation (MR !46)
- KratosSolverWrapper only required attribute for matching QoI with moment estimators is qoiEstimator. (MR !51)
- The Kratos analysis stage is imported with the `analysisStage` key of the `KratosSolverWrapper` class. It is no longer necessary to pass its path to the `pythonpath` flag of `runcompss`. (MR !44)

### Fixed
- MLMC with refinement strategy "deterministic adaptive refinement" works. (MR !48)
- KratosSolverWrapper tasks returnZeroQoiAndTime_Task and PostprocessContributionsPerInstance accept a general positioning of moment estimators, and not a fixed order. (MR !51)
- KratosSolverWrapper can now be imported without raising `ModuleNotFoundError`. (#44, !44)
- Removed many Kratos warnings, as mentioned in #55. (!58)

### Deprecated

### Removed
- KratosSolverWrapper keys "numberMomentEstimator", "numberCombinedMomentEstimator", "numberMultiMomentEstimator", "numberMultiCombinedMomentEstimator" are removed and replaced by "qoiEstimator". (MR !51)
- MonteCarloIndex no longer supports deprecated keyword arguments "combinedEstimator" and "combinedEstimatorInputDictionary"; use qoiEstimator and qoiEstimatorInputDictionary.
- The key `problemId` of the `KratosSolverWrapper` class is removed. (MR !54)

## [2.0.0] — 2020-11-10

### Added
- Updating moment estimators in small batches (see `MonteCarloIndex.eventGroupSize`) (MR !15).
- Asynchronous algorithms for single- and multi-level Monte Carlo methods (MR !16)
- Estimation of 'combined' expectation (time and events) for random processes (MR !19)
- Support for multi-valued (e.g. vectors) random variables (issue #31, MR !36)
- Estimation of moments of order 3 and 4, with a posteriori estimation of statistical error (MR !36)
- Examples, both built-in and using Kratos as external solver (MR !38)
- Test cases
- README (MR !33)
- Changelog (MR !42)
- New features of the Kratos interface:
  - different meshing strategies (support distributed environments)
  - write to file in distributed environments

### Changed
- Documentation re-created anew, from docstrings (MR !31, !32, !41)
- The choice of framework for distributed computing is contained in a single module definition (issue #30, MR !30)

### Fixed
- Excessive memory usage in case of high number of estimators (MR !23).

### Deprecated
∅

### Removed
∅


## [1.0.0] — 2019-05-31

Initial release.

### Added
- Single- and multi-level Monte Carlo estimation of expectation and variance of real-valued random variable
- Adaptivity of number of samples and levels based on a posteriori error estimators
- Stopping criterion based on a posteriori error estimators
- Interface with external solver: Kratos
- Parallelisation using the Python interface common to COMPSs and HyperLoom
